import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';

import Register from '../Register';

describe('Register Form', () => {
    it('Check Tab one', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('#simple-tabpanel-0')).toHaveLength(1);
    });
    it('Check Tab two', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('#simple-tabpanel-1')).toHaveLength(1);
    });
    it('Check Tab three', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('#simple-tabpanel-2')).toHaveLength(1);
    });
    it('Check Tab four', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('#simple-tabpanel-3')).toHaveLength(1);
    });
});
