import React from 'react';
import Navbar from '../components/Navbar/Navbar'
import { withStyles } from "@material-ui/core/styles";
import Background from '../../images/1.jpg'
import Container from '@material-ui/core/Container';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import Transacation from '../components/Transcation/Transcation'
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BASE_URL } from '../../config/env'


/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: '50px'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    paper: {
        //marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        // [theme.breakpoints.up('lg')]: {
        backgroundColor: 'white',
        padding: '20px',
        borderRadius: '30px'
        //},

    },
    pageBackground: {
        backgroundImage: 'url(' + Background + ')',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'normal',
        alignItems: 'center',
    },
    table: {
        minWidth: 300,
        //'& thead':{
        //  }
        '& thead': {
            backgroundColor: 'mediumseagreen',
            color: theme.palette.common.white,
        },
        '& tbody >tr:nth-child(even)': {
            backgroundColor: 'grey'
        },
        '& tbody >tr:nth-child(odd)': {
            backgroundColor: 'silver'
        }
    },

});




export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            statements: []
        }
    }

    componentDidMount() {
        axios.get(`${BASE_URL}/customers/${JSON.parse(localStorage.getItem('user')).customerId}/transactions`).then(res => {
            this.setState({
                statements: res.data
            })
        })
    }


    renderTableHeader = () => {
        return (
            <TableRow>
                <TableCell>Transaction Id</TableCell>
                <TableCell >Transactional Amount</TableCell>
                <TableCell >Transaction Date</TableCell>
                <TableCell >Account Type</TableCell>
                <TableCell>Transaction Type</TableCell>
            </TableRow>
        )
    }

    renderTableBody = (statements) => {

        return (
            <React.Fragment>
                {
                    statements.map((statement) => (
                        <TableRow key={statement.transactionId}>
                            <TableCell component="th" scope="row">
                                {statement.transactionId}
                            </TableCell>
                            <TableCell >  {statement.transactionAmount}</TableCell>
                            <TableCell >{statement.transactionDate.toString()}</TableCell>
                            <TableCell >{statement.accountType}</TableCell>
                            <TableCell >{statement.transactionType}</TableCell>
                        </TableRow>

                    ))
                }
            </React.Fragment>
        )
    }




    render() {
        const { classes } = this.props
        const { statements } = this.state

        return (
            <React.Fragment>
                <Navbar
                    appBarColor={'rgb(138,198,58)'}
                    appTitle='Mortage'
                    {...this.props}
                />
                <Container >
                    {/* <Transacation accountNumber={'100'} balance={'50'} classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody()} /> */}
                    {/* <Statement classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody()} /> */}
                    <div className={classes.root}>
                        {
                            statements.length > 0 &&
                            <React.Fragment>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography className={classes.heading}>Customer Account Statment</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Transacation accountNumber={statements[0].accountNumber} balance={statements[0].balance} classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody(statements[0].transactionDtoList)} />
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header-1"
                                    >
                                        <Typography className={classes.heading}>Mortage Account Statment</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Transacation accountNumber={statements[1].accountNumber} balance={statements[1].balance} classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody(statements[1].transactionDtoList)} />
                                    </AccordionDetails>
                                </Accordion>
                            </React.Fragment>
                        }
                    </div>

                </Container>

            </React.Fragment>
        );
    }
}


export default withStyles(styles, { withTheme: true })(Login);
