import React from 'react';
import { withStyles } from "@material-ui/core/styles";
import TabLayout from '../components/Tab/TabLayout'
import axios from 'axios';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import Grid from '@material-ui/core/Grid';
import Logo from '../components/Logo/Logo'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import Popup from '../components/Popup/Popup'
import { BASE_URL } from '../../config/env'

const styles = theme => ({
    root: {

    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: '20px',
        borderRadius: '15px'
    },
    pageBackground: {
        backgroundColor: 'rgb(138,198,58)',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',

        //
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
});

/**
 * Register COntainer
 */

export class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            currentTab: 0,
            //tab 1
            mortagagePurpose: 'home loan',
            peopleCount: "1",
            propertyCost: 100000,
            deposit: 1000,
            //tab 2
            employeeStatus: 'employeed',
            occupation: '',
            contractType: '',
            employmentDate: 10,
            employmentMonth: 10,
            employmentYear: 1990,
            //tab 3
            title: 'Mr',
            fname: '',
            lname: '',
            mname: '',
            applicantDate: 10,
            applicantMonth: 10,
            applicantYear: 1990,
            //tab 4
            phoneNumber: '',
            email: '',
            confirmEmail: '',
            errors: [],
            showUserDetails: false,
            userDetails: {},
        }
    }

    /** 
     * @param {*} e 
     * Changing state for input field
     */
    _handleTextFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    /**
     * Layout for Tab1
     */

    tabOneLayout = () => {
        const { classes } = this.props
        const { errors } = this.state
        return (
            <Container >
                <FormControl fullWidth variant="outlined" className={classes.formControl}>
                    <Typography component="p" variant="overline">
                        I am thinking about
                   </Typography>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={this.state.mortagagePurpose}
                        onChange={this._handleTextFieldChange}
                        name='mortagagePurpose'
                    //label="Age"
                    >
                        <MenuItem value={'home loan'}>Buying my first home</MenuItem>
                        <MenuItem value={'car loan'}>Buying my first car</MenuItem>
                    </Select>
                    <Typography component="p" variant="overline">
                        How many peoples applying for ?
                   </Typography>
                    <RadioGroup row aria-label="gender" value={this.state.peopleCount} name='peopleCount' onChange={this._handleTextFieldChange}>
                        <FormControlLabel value={"1"} control={<Radio />} label="1" />
                        <FormControlLabel value={"2"} control={<Radio />} label="2" />
                        <FormControlLabel value={"3"} control={<Radio />} label="3+" />
                    </RadioGroup>
                    <Typography component="p" variant="overline">
                        How much you think property will cost ?
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        //label="Email"
                        name="propertyCost"
                        type="number"
                        //autoComplete="username"
                        value={this.state.propertyCost}
                        onChange={this._handleTextFieldChange}
                        autoFocus

                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AttachMoneyIcon />
                                </InputAdornment>
                            ),
                            inputProps: {
                                min: 100000
                            }
                        }}

                    />
                    <Typography component="p" variant="overline">
                        How much deposit will have?
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        type='number'
                        //label="Email"
                        name="deposit"
                        value={this.state.deposit}
                        //autoComplete="username"
                        //value={email}
                        // onChange={this._handleTextFieldChange}
                        autoFocus
                        onChange={this._handleTextFieldChange}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AttachMoneyIcon />
                                </InputAdornment>
                            ),
                        }}
                    //error={onSubmit && email.length <= 0}
                    />

                </FormControl>
                {errors.length > 0 && this.renderValidationError(errors)}
                <div>
                    <Button

                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(1) }}
                    >
                        Next
                </Button>
                </div>
            </Container>
        )
    }

    /**
     * Layout for Tab2
     */

    tabTwoLayout = () => {
        const { classes } = this.props
        const { errors } = this.state
        return (
            <Container >
                <FormControl fullWidth variant="outlined" className={classes.formControl}>
                    <Typography component="p" variant="overline" >
                        What is your Employment Status ?
                   </Typography>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        // value={1}
                        onChange={this._handleTextFieldChange}
                        name='employeeStatus'
                        value={this.state.employeeStatus}
                    >
                        <MenuItem value={'employeed'}>Employed</MenuItem>
                        <MenuItem value={'unemployeed'}>UnEmployed</MenuItem>
                    </Select>
                    <Typography component="p" variant="overline">
                        What is your Occupation ?
                   </Typography>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        onChange={this._handleTextFieldChange}
                        name='occupation'
                        value={this.state.occupation}
                    >
                        <MenuItem value={"Business"}>Business</MenuItem>
                        <MenuItem value={"Farmer"}>Farmer</MenuItem>
                    </Select>

                    {
                        this.state.employeeStatus === 'employeed' && <React.Fragment>
                            <Typography component="p" variant="overline">
                                What is your contract type ?
                   </Typography>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={this._handleTextFieldChange}
                                name='contractType'
                                value={this.state.contractType}
                            >
                                <MenuItem value={'Permanent'}>Permanent</MenuItem>
                                <MenuItem value={'Contract'}>Contract</MenuItem>
                            </Select>
                        </React.Fragment>
                    }

                    {
                        this.state.employeeStatus === 'employeed' && <React.Fragment>
                            <Typography component="p" variant="overline">
                                When did u start new Job ?
                   </Typography>
                            <div>
                                <TextField
                                    variant="outlined"
                                    required
                                    style={{ width: "10%" }}

                                    autoFocus
                                    type='number'
                                    onChange={this._handleTextFieldChange}
                                    name='employmentDate'
                                    value={this.state.employmentDate}
                                />
                                <span style={{ width: "2%" }}> </span>
                                <TextField
                                    variant="outlined"
                                    required
                                    style={{ width: "10%" }}

                                    autoFocus
                                    type='number'
                                    name='employmentMonth'
                                    onChange={this._handleTextFieldChange}
                                    value={this.state.employmentMonth}
                                />

                                <span style={{ width: "2%" }}> </span>
                                <TextField
                                    variant="outlined"
                                    required
                                    style={{ width: "10%" }}
                                    autoFocus
                                    type='number'
                                    name='employmentYear'
                                    onChange={this._handleTextFieldChange}
                                    value={this.state.employmentYear}
                                />

                            </div>

                        </React.Fragment>
                    }

                </FormControl>
                {errors.length > 0 && this.renderValidationError(errors)}
                <div>

                    <Button

                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(0) }}
                    >
                        Previous
                </Button>
                    <Button

                        style={{ marginLeft: '5px' }}
                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(2) }}
                    >
                        Next
                </Button>
                </div>
            </Container>
        )
    }

    /**
     * Layout for Tab3
     */
    tabThreeLayout = () => {
        const { classes } = this.props
        const { errors } = this.state
        return (
            <Container >
                <FormControl fullWidth variant="outlined" className={classes.formControl}>
                    <Typography component="p" variant="overline">
                        Title
                   </Typography>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"

                        onChange={this._handleTextFieldChange}
                        name='title'
                        value={this.state.title}
                    >
                        <MenuItem value={'Mr'}>Mr</MenuItem>
                        <MenuItem value={'Mrs'}>Mrs</MenuItem>
                    </Select>
                    <Typography component="p" variant="overline">
                        First Name
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        onChange={this._handleTextFieldChange}
                        name='fname'
                        value={this.state.fname}
                        //label="Email"
                        //name="email"
                        //autoComplete="username"
                        // value={email}
                        //onChange={this._handleTextFieldChange}
                        autoFocus
                    // error={onSubmit && email.length <= 0}

                    />
                    <Typography component="p" variant="overline">
                        Middle Name
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        onChange={this._handleTextFieldChange}
                        name='mname'
                        value={this.state.mname}
                        //label="Email"
                        //name="email"
                        //autoComplete="username"
                        // value={email}
                        //onChange={this._handleTextFieldChange}
                        autoFocus
                    // error={onSubmit && email.length <= 0}
                    />
                    <Typography component="p" variant="overline">
                        Last Name
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        onChange={this._handleTextFieldChange}
                        name='lname'
                        value={this.state.lname}
                        autoFocus
                    />
                    <Typography component="p" variant="overline">
                        DOB
                   </Typography>
                    <div>
                        <TextField
                            variant="outlined"
                            required
                            style={{ width: "10%" }}

                            autoFocus
                            onChange={this._handleTextFieldChange}
                            name='applicantDate'
                            value={this.state.applicantDate}
                        />
                        <span style={{ width: "2%" }}> </span>
                        <TextField
                            variant="outlined"
                            required
                            style={{ width: "10%" }}
                            onChange={this._handleTextFieldChange}
                            name='applicantMonth'
                            value={this.state.applicantMonth}
                            autoFocus
                        />
                        <span style={{ width: "2%" }}> </span>
                        <TextField
                            variant="outlined"
                            required
                            style={{ width: "10%" }}
                            onChange={this._handleTextFieldChange}
                            name='applicantYear'
                            value={this.state.applicantYear}
                            autoFocus
                        />
                    </div>


                </FormControl>
                {errors.length > 0 && this.renderValidationError(errors)}
                <div>

                    <Button

                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(1) }}
                    >
                        Previous
                 </Button>
                    <Button

                        style={{ marginLeft: '5px' }}
                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(3) }}
                    >
                        Next
</Button>
                </div>
            </Container>
        )
    }

    /**
     * Layout for Tab4
     */
    tabFourLayout = () => {
        const { classes } = this.props
        const { errors } = this.state
        return (
            <Container >
                <FormControl fullWidth variant="outlined" className={classes.formControl}>
                    <Typography component="p" variant="overline">
                        Contact Number
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        type='number'
                        //label="Email"
                        //name="email"
                        //autoComplete="username"
                        // value={email}
                        //onChange={this._handleTextFieldChange}
                        autoFocus
                        onChange={this._handleTextFieldChange}
                        name='phoneNumber'
                        value={this.state.phoneNumber}
                    // error={onSubmit && email.length <= 0}
                    />
                    <Typography component="p" variant="overline">
                        What is your email address ?
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        onChange={this._handleTextFieldChange}
                        name='email'
                        value={this.state.email}
                        //label="Email"
                        //name="email"
                        //autoComplete="username"
                        // value={email}
                        //onChange={this._handleTextFieldChange}
                        autoFocus
                    // error={onSubmit && email.length <= 0}
                    />
                    <Typography component="p" variant="overline">
                        Please confirm the mail ddress ?
                   </Typography>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        onChange={this._handleTextFieldChange}
                        name='confirmEmail'
                        value={this.state.confirmEmail}
                        //label="Email"
                        //name="email"
                        //autoComplete="username"
                        // value={email}
                        //onChange={this._handleTextFieldChange}
                        autoFocus
                    // error={onSubmit && email.length <= 0}
                    />

                </FormControl>
                {errors.length > 0 && this.renderValidationError(errors)}
                <div>
                    <Button

                        variant="contained"
                        color="primary"
                        onClick={() => { this.onTabChange(2) }}
                    >
                        Previous
                 </Button>
                    <Button

                        style={{ marginLeft: '5px' }}
                        variant="contained"
                        color="primary"
                        onClick={() => { this.onSubmit() }}
                    >
                        Submit
</Button>
                </div>

            </Container>
        )
    }


    /**
     * 
     * @param {*} tabNo 
     * on Tb CHnge
     */

    onTabChange = (tabNo) => {
        const { currentTab } = this.state

        if (currentTab === 0) {
            let errors = this.checkTabOneValidation()
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })

                return;
            }
        }

        if (currentTab === 1) {
            let errors = this.checkTabTwoValidation()
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })

                return;
            }
        }

        if (currentTab === 2) {
            let errors = this.checkTabThreeValidation()
            if (errors.length > 0) {
                this.setState({
                    errors: errors
                })

                return;
            }
        }

        this.setState({
            currentTab: tabNo,
            errors: []
        })
    }


    /**
     * Validation for Tab 1
     */
    checkTabOneValidation = () => {
        const { deposit, propertyCost } = this.state
        let errors = []
        if (propertyCost !== null && propertyCost < 100000) {
            errors.push('Property cost should be greater than 100000')
        }
        if (deposit !== null && deposit < 0) {
            errors.push('deposit should be greatee thn zero')
        }

        return errors
    }

    /**
    * Validation for Tab 2
    */

    checkTabTwoValidation = () => {
        const { employeeStatus,occupation, contractType, employmentDate, employmentMonth, employmentYear } = this.state
        let errors = []

        if (employeeStatus === 'employeed') {
            if (contractType === '') {
                errors.push('Please Choose Contract type')
            }
            if (parseInt(employmentDate) <= 0 ||
                parseInt(employmentDate) > 31 || parseInt(employmentMonth) <= 0 || parseInt(employmentMonth) > 12 || parseInt(employmentYear) < 1990) {
                errors.push('Invalid Job Start Date')
            }
        }

        if (occupation === '') {
            errors.push('Please Select Occupation')
        }

        return errors
    }

    /**
    * Validation for Tab 3
    */

    checkTabThreeValidation = () => {

        const { fname, mname, lname, applicantDate, applicantMonth, applicantYear } = this.state;
        let errors = []

        if (fname === '' || lname === '' || mname === '') {
            errors.push('Invalid name')
        }
        if (parseInt(applicantDate) <= 0 ||
            parseInt(applicantDate) > 31 || parseInt(applicantMonth) <= 0 || parseInt(applicantMonth) > 12 || parseInt(applicantYear) < 1990) {
            errors.push('Invalid Date of Birth')
        }
        return errors
    }

    /**
    * Validation for Tab 4
    */

    checkTabFourValidation = () => {
        const { phoneNumber, email, confirmEmail } = this.state
        let errors = []
        if (!this.validatePhoneNumver(phoneNumber)) {
            errors.push('Invalid mobile number')
        }
        if (!this.validateEmail(email)) {
            errors.push('Invalid Email Format')
        }
        if (email !== confirmEmail) {
            errors.push('email and confirm email is wrong')
        }

        return errors
    }


    validatePhoneNumver = (number) => {
        const re = /^[6-9]\d{9}$/;
        return re.test(number);
    }

    /**
     * 
     * @param {*} email
     * Email validation 
     */
    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    /**
     * 
     * @param {*} errors 
     * Renders validation error
     */
    renderValidationError = (errors) => {
        return <React.Fragment>
            {errors.map((error, index) => {
                return <div style={{ color: 'red', fontWeight: 'bold' }}>*{error}</div>
            })}
        </React.Fragment>
    }


    /**
     * On Submit
     */
    onSubmit = () => {
        let errors = this.checkTabFourValidation()
        if (errors.length > 0) {
            this.setState({
                errors: errors
            })

            return;
        }

        // let request = {
        //     title: this.state.title,
        //     customerName: `${this.state.fname}''${this.state.mname}''${this.state.lname}`,
        //     customerDob: `${this.state.applicantDate}/${this.state.applicantMonth}/${this.state.applicantYear}`,
        //     phoneNumber: this.state.phoneNumber,
        //     customerEmail: this.state.email,
        //     mortagagePurpose: this.state.mortagagePurpose,
        //     propertyCost: this.state.propertyCost,
        //     deposit: this.state.deposit,
        //     occupation: this.state.occupation,
        //     contractType: this.state.contractType,
        //     employmentDate: `${this.state.employmentDate}/${this.state.employmentMonth}/${this.state.employmentYear}`,
        // }

        axios.post(`${BASE_URL}/customers`, {
            title: this.state.title,
            customerName: `${this.state.fname}''${this.state.mname}''${this.state.lname}`,
            customerDob: `${this.state.applicantDate}/${this.state.applicantMonth}/${this.state.applicantYear}`,
            phoneNumber: this.state.phoneNumber,
            customerEmail: this.state.email,
            mortagagePurpose: this.state.mortagagePurpose,
            propertyCost: this.state.propertyCost,
            deposit: this.state.deposit,
            employmentStatus: this.state.employeeStatus,
            firstName: this.state.fname,
            lastName: this.state.lname,
            occupation: this.state.occupation,
            contractType: this.state.contractType,
            employmentDate: `${this.state.employmentDate}/${this.state.employmentMonth}/${this.state.employmentYear}`,
        }).then((data) => {
            this.setState({
                showUserDetails: true,
                userDetails: data.data
            })
        }).catch(err => {
            console.log('------->', err.response)
            if (err.response.data.statuscode === 4041) {
                this.setState({
                    errors: [err.response.data.message]
                })
            }
        })
    }

    renderModalDescription = () => {
        const { userDetails } = this.state
        return (
            <div>
                <div><span>Customer ID :</span><span style={{ fontWeight: "bold" }}>{userDetails.customerId}</span></div>
                <div><span>Customer Login Id :</span><span style={{ fontWeight: "bold" }}>{userDetails.customerLoginId}</span></div>
                <div><span>Password :</span><span style={{ fontWeight: "bold" }}>{userDetails.password}</span></div>
                <div><span>Mortage Account Number:</span><span style={{ fontWeight: "bold" }}>{userDetails.mortgageAccountNumber}</span></div>
                <div><span>Current Account Number:</span><span style={{ fontWeight: "bold" }}>{userDetails.currentAccountNumber}</span></div>
                <Grid container>
                    <Grid item>
                        <Link href="/" variant="body2">
                            {"Click here to Login"}
                        </Link>
                    </Grid>
                </Grid>
            </div>
        )
    }


    render() {
        const { classes } = this.props;
        const { currentTab } = this.state


        return (
            <div className={classes.pageBackground}>
                <Logo />
                <Container >
                    <TabLayout
                        backgroundColor={'mediumseagreen'}
                        currentTab={currentTab}
                        onTabChange={this.onTabChange}
                        tabOneLayout={this.tabOneLayout()}
                        tabOneLabel={'Basic Details'}
                        tabTwoLayout={this.tabTwoLayout()}
                        tabTwoLabel={'Employment'}
                        tabThreeLayout={this.tabThreeLayout()}
                        tabThreeLabel={'Applicant'}
                        tabFourLayout={this.tabFourLayout()}
                        tabFourLabel={'Contact Details'}
                    />
                    <Popup handleClose={() => { this.setState({ showUserDetails: false }) }} isOpen={this.state.showUserDetails} modalTitle={'Your Account Details.Please make note '} modalDescription={this.renderModalDescription()} />
                </Container>

            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Register);


