import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from "@material-ui/core/styles";

import Statement from '../Table/Table'

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },

});



function Transcation(props) {
    const { classes } = props
    return (
        <div>
            <Typography component="h1" variant="h5">
                Account Number - {props.accountNumber}
            </Typography>
            <Typography component="h1" variant="h5">
                Balance = <span style={{ color: props.balance < 0 ? 'red' : 'green' }}>{props.balance}</span>
            </Typography>
            <Statement classes={classes} tableHeader={props.tableHeader} tableBody={props.tableBody} />
        </div>
    );
}

export default withStyles(styles, { withTheme: true })(Transcation);

