import React, { Suspense } from 'react';

import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.css';
import PrivateRoute from './js/components/PrivateRoute'
const Login = React.lazy(() => import('./js/containers/Login'));
const Register = React.lazy(() => import('./js/containers/Register'));
const Dashboard = React.lazy(() => import('./js/containers/Dashboard'));

// const Dashboard = React.lazy(() => import('./js/containers/Dashboard'));
// const WatchedMovies = React.lazy(() => import('./js/containers/Watched'));
// const Cart = React.lazy(() => import('./js/containers/Cart'))
// const Orders = React.lazy(() => import('./js/containers/Orders'))



function App() {

  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/register" component={Register} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
             {/*<PrivateRoute path="/dashboard" component={Dashboard} />
            <PrivateRoute path="/watched" component={WatchedMovies} /> */}
          </Switch>
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
